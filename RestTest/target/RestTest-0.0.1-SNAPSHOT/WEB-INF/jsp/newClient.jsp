<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
  <head>
  
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


    <title>Spring MVC - Ajax</title>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/scripts.js"/>"></script>
    <style>
     body { background-color: #eee; font: helvetica; }
     #container { width: 500px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
     .green { font-weight: bold; color: green; }
     .message { margin-bottom: 10px; }
     label { width:70px; display:inline-block;}
     .hide { display: none; }
     .error { color: red; font-size: 0.8em; }
    </style>
  </head>
  <body>


<h2>Submit new Client from controller</h2>	 
<form:form method="POST" action="${path}/saveClient/" modelAttribute="client" id="newClientForm">
				
				<%-- Autoassigned anyway
				<form:label path="id"></form:label>
				<form:input path="id"/>
				 --%>
				
				<br>
				<form:label path="address"></form:label>
				<form:input path="address"/>
				<br>
				<form:label path="name"></form:label>
				<form:input path="name"/>
				<br>
				<form:label path="surname"></form:label>
				<form:input path="surname"/>
				
				<input type="submit" value="Save" id="submit"/>

</form:form>









	
  </body>
</html>