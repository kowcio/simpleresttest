<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html;charset=UTF-8"%>

<!DOCTYPE HTML>
<html>
  <head>
  
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


    <title>Spring MVC - Ajax</title>
    
    <link href="<c:url value="/resources/css/all.css"/>" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/scripts.js"/>"></script>
    
    
    <style>
     body { background-color: #eee; font: helvetica; }
     #container { width: 500px; background-color: #fff; margin: 30px auto; padding: 30px; border-radius: 5px; box-shadow: 5px; }
     .green { font-weight: bold; color: green; }
     .message { margin-bottom: 10px; }
     label { width:70px; display:inline-block;}
     .hide { display: none; }
     .error { color: red; font-size: 0.8em; }
    </style>
  </head>
  <body>

<div id="menu">
<form:form method="POST" action="${path}/saveClient/" modelAttribute="client" id="clientform">
				<h2>Generate klient ID from given data<br /> </h2>
				 <h5>Data must match : A-Za-z, 0-9,' ', -, 3-30 chars</h5>	
				
				<form:label path="address">Adres</form:label>
				<form:input path="address"/>
				<br>
				<form:label path="name">Imię</form:label>
				<form:input path="name"/>
				<br>
				<form:label path="surname">Surname</form:label>
				<form:input path="surname"/>
				<br>
				<div id="addressValid"></div>
				<div id="nameValid"></div>
				<div id="surnameValid"></div>
				
				<input type="submit" value="Save client" id="submitClient"/>
</form:form>



<h2>Submit new order </h2>	 
<form:form method="POST" action="${path}/saveorder" modelAttribute="order" id="orderForm">
<%-- 				<form:label path="id">ID Autoassigned anyway</form:label>
				<form:input path="id"/> --%>
				<br>
				<form:label path="clientID">ID Klienta</form:label>
				<form:input path="clientID"/>
				<br>
				<form:label path="dateOrder">Data Zam</form:label>
				<form:input path="dateOrder" readonly="true"/>
				<br>
				<form:label path="statusHistoryList">Status</form:label>
				<form:input path="statusHistoryList" readonly="true"/>
				<br>	
				<form:label name="itemsList" path="itemsList">Lista zak.</form:label>
				<form:input name="itemsList" path="itemsList"/>										

				<br> 
				<input type="submit" value="Save Order" id="submitOrder"/>	
</form:form>


<br />

orderStatus: <input id="orderStatus" type="text" placeholder="Input order ID to get status history"/>	
<button id="getOrderStatus"> Get orderStatus</button>
<br />

cancelStatus: <input id="cancelStatus" type="text" placeholder="Input order ID to change status"/> 
<button id="getCancelOrder"> Cancel order</button>
<br />

modifyOrderStatus: <br />
<input id="modifyStatus" type="text" placeholder="Input order id ot change status"/>
	<select id="selectOrderStatus">
	  <option value="new">new</option>
	  <option value="inprogress">inprogress</option>
	  <option value="onhold">onhold</option>
	  <option value="canceled">canceled</option>
	  <option value="delivered">delivered</option>
	</select> 
<button id="getModifyOrder"> modify Order</button>
	
<br />		<br />
		
Modify orderItems: 		<br/>
ID : <input id="modifyItemsOrderID" type="text"/>
Items: <input id="modifyItems" type="text"/><br />
<button id="loadItems"> Load Items</button>
<button id="saveItemsForID"> Save Items</button>
		
		
		
		
		
</div>		
		
	
<div id="response"></div>
		










	
  </body>
</html>