package rest.controllers;

import java.util.List;

import javax.validation.Valid;
import javax.xml.registry.InvalidRequestException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rest.daos.ClientDAO;
import rest.daos.OrderDAO;
import rest.entities.Client;
import rest.entities.Client;
import rest.entities.Order;
import rest.service.OrderService;

/**
 * Controller used to manage send objects with rest OrderService. 
 * @author pk
 *
 */
@Controller
public class RestFormController {

	Logger log = Logger.getLogger("InspireLogger");
	
	OrderService orderService;

	@Autowired
	public RestFormController (OrderService orderService) {
		this.orderService = orderService;
	}

	
	@RequestMapping(value = "saveorder", method = RequestMethod.POST)
	@ResponseBody
	public String saveOrder( Order order)
			 {
		log.info("zapisywanie zamowienia \n\n" + order.toString());
		order = orderService.saveOrder(order);
		return "Saved order: " + order.toString();
	}
	
	
	@RequestMapping(value = "saveClient", method = RequestMethod.POST)
	@ResponseBody
	//Ajax hibernate contraint validation in JS
	public Client saveClient(Client client		) {
		log.info("zapisywanie clienta = " + client.toString());
		ClientDAO cdao = new ClientDAO();
		client = cdao.saveClient(client);
		return client;
	}
	
	
	
	@RequestMapping(value = "orderStatus/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getStatus(@PathVariable(value="id") String id) {
		int int_id = Integer.parseInt(id);
		log.info("ID = " + int_id);
		List<String> sl = 
				orderService.getStatus(int_id).getStatusHistoryList();
		
		//addformat 
		String format = sl.toString();
		format=format.replaceAll("#Date:","<br />#Date:");
		
		return "Order status  ="+ format;
	}
	
	

	@RequestMapping(value = "modify/{id}/{status}", method = RequestMethod.GET)
	@ResponseBody
	public String modifyStatus(
			@PathVariable(value="id") String id,
			@PathVariable(value="status") String status
			) {
		log.info("Modyfing order controller : id "+id +" with status = "+ status);
		int int_id = Integer.parseInt(id);
		Order order = orderService.modifyOrder(int_id, status);
		return "Order status  = "+ order.getStatusHistoryList().toString();
	}

	
	
	
	
	@RequestMapping(value = "getOrder/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Order getOrder(
			@PathVariable(value="id") String id			) {
		log.info("Getting order controller : id "+id  );
		int int_id = Integer.parseInt(id);
		Order order = orderService.getOrderByID(int_id);
		return order;
	}

	
	
	@RequestMapping(value = "saveItemsList/{id}/{list}", method = RequestMethod.POST)
	@ResponseBody
	public Order modifyLisyt(
			@PathVariable(value="id") String id,
			@PathVariable(value="list") String list
			) {
		log.info("Modyfing order controller : id "+id +" with list = "+ list);
		int int_id = Integer.parseInt(id);
		Order order = orderService.modifyList(int_id, list);
		return order;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	

}