package rest.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import rest.daos.OrderDAO;
import rest.entities.Client;
import rest.entities.Order;
import rest.service.OrderService;

@Controller
public class FormController {

	Logger log = Logger.getLogger("InspireLogger");

	OrderService orderService;

	@Autowired
	public FormController(OrderService orderService) {
		this.orderService = orderService;
	}

	/**
	 * Main page controller for the testing rest service
	 * 
	 * @return
	 */
	@RequestMapping(value = "saveRest", method = RequestMethod.GET)
	public ModelAndView homeSaveRestForm() {
		log.info("RestFormController");

		ModelAndView mv = new ModelAndView("homeSaveRest");

		Order order = Order.RandomOrder();
		Client client = new Client();

		log.info("Returning the saveRest form");
		// populating with basicdata for model attributes in forms
		mv.addObject("client", client);
		mv.addObject("order", order);
		mv.addObject("modifyOrder", order);

		return mv;
	}

	@RequestMapping(value = "rest", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView homeRestForm() {
		ModelAndView mv = new ModelAndView("homeRest");
		// mv.addObject("person", new Person());
		return mv;
	}

	@RequestMapping(value = "newClient", method = RequestMethod.GET)
	public ModelAndView newClientForm() {
		ModelAndView mv = new ModelAndView("newClient");
		mv.addObject("client", new Client());
		return mv;
	}

}