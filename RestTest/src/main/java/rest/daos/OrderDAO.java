package rest.daos;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import rest.entities.Order;

/**
 * MAIN DAO class. Use contraints and etc.
 * 
 * @author Kowcio
 * 
 */
public class OrderDAO {

	Logger log = Logger.getLogger("InspireLogger");

	HibGetDBSession fabrykaHibernejta = new HibGetDBSession();

	public Session getNewSession() {
		return fabrykaHibernejta.getNewSessionAnnotations();
	}

	/**
	 * Save a Order class into the user table
	 * 
	 * @param order
	 *            Order
	 * @return
	 */

	public Order saveOrder(Order order) {
		try {
			Session session = getNewSession();
			log.info("Saving Record");

			Transaction tx = session.beginTransaction();
			// setting the date of adding the Order
			session.save(order);

			tx.commit();
			log.info("Order id = " + order.getId() + " added !");
			return order;
		} catch (Exception e) {
			log.info("Cos sie wysypalo - saveOrder");
			e.printStackTrace();
			return order;
		}

	}

	/**
	 * 
	 * @return All Orders as list.
	 */

	@SuppressWarnings("unchecked")
	public List<Order> getAllOrders() {
		List<Order> OrdersList = null;
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			OrdersList = (List<Order>) session.createQuery("from Order").list();
			log.info("OrdersList count = " + OrdersList.size());
			tx.commit();
		} catch (Exception e) {
			log.info("Cos sie wysypalo - OrderDAO - getAllOrders");
			e.printStackTrace();
		}

		return OrdersList;
	}

	/**
	 * Return Order by ID.
	 * 
	 * @param OrderId
	 *            - Orders ID which we wish to load.
	 * @return Specified Order
	 */

	public Order getOrderByID(int OrderId) {
		Order Order = new Order();
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			Order = (Order) session.get(Order.class, OrderId);
			tx.commit();
			return Order;
		} catch (Exception e) {
			log.info("Cos sie wysypalo - getOrderByID");
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * Update a Order object in db.
	 * 
	 * @param Order
	 *            Order which we wish to update. Needs to have a specified ID.
	 */

	public void updateOrder(Order Order) {
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			session.update(Order);
			tx.commit();
		} catch (Exception e) {
			log.info("Cos sie wysypalo - getOrderByID");
			e.printStackTrace();
		}
	}

	/**
	 * Delete in db a Order object by given ID.
	 * 
	 * @param Needs
	 *            to have a specified ID.
	 */

	public void deleteOrder(int id) {
		try {
			Session session = getNewSession();
			OrderDAO Order = new OrderDAO();
			Transaction tx = session.beginTransaction();
			session.delete(Order.getOrderByID(id));
			tx.commit();

		} catch (IllegalArgumentException e) {
			log.info(" No Order by that id");
			e.printStackTrace();
		} catch (Exception e) {
			log.info("Cos sie wysypalo - DeleteOrderByID");
		}
	}

	/**
	 * Delete in db a Order object by given URL.
	 * 
	 * @param Needs
	 *            to have a specified URL.
	 */
	@Deprecated
	public void deleteOrderByURLBad(String url) {
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			String query = "delete from Order where url='" + url + "'";
			log.info("Delete query = " + query);
			session.createQuery(query);
			tx.commit();
		} catch (Exception e) {
			log.info("Cos sie wysypalo - deleteOrderByURL");
			e.printStackTrace();
		}
		log.info("Deleted Order by URL=" + url);
	}

	/**
	 * Delete in db a Order object by given URL.
	 * 
	 * @param Needs
	 *            to have a specified URL.
	 * @return true if all ok
	 */
	public boolean deleteOrderByURL(String url) {
		Session session = getNewSession();
		Transaction transaction = session.beginTransaction();
		try {
			// your code
			String hql = "delete from Order where url= :url";
			Query query = session.createQuery(hql);
			query.setString("url", url);
			log.info(query.executeUpdate());
			// your code end

			transaction.commit();
			return true;
		} catch (Exception e) {
			transaction.rollback();
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * Delete in db a Order object by given URL.
	 * 
	 * @param Needs
	 *            to have a specified URL.
	 */
	@SuppressWarnings("unchecked")
	public Order getOrderByURL(String url) {
		List<Order> ml = null;
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			log.info("Before delete");
			ml = (List<Order>) session.createQuery(
					"from Order where url='" + url + "'").list();
			log.info("After delete");
			tx.commit();
			return ml.get(0);
		} catch (Exception e) {
			log.info("Cos sie wysypalo " + e.getCause());
			// e.printStackTrace();
			return null;
		}

	}

	/**
	 * Get number of Orders in db
	 * 
	 * @param viewType
	 * @return
	 */
	public int getPostsNumberInDB() {
		long pcl = 0;
		try {
			Session session = getNewSession();
			// postCount
			pcl = (Long) session.createCriteria(Order.class)
					.setProjection(Projections.rowCount()).uniqueResult();
			return safeLongToInt(pcl);
		} catch (Exception e) {
			log.info("Exception error - PostDAO - getPostsNumberInDB");
			e.printStackTrace();
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	public List<Order> getDisplayedOrders() {

		List<Order> motList = null;
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			motList = (List<Order>) session.createQuery(
					"from Order where displayed = '1'").list();
			log.info("motList count = " + motList.size());
			tx.commit();
			return motList;
		} catch (Exception e) {
			log.info("Exception error - PostDAO - getAllNotepadPosts");
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Order> getDisplayedSpecifiedTypeOrders(String linkType) {
		List<Order> postsList = null;
		try {
			Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			postsList = (List<Order>) session.createQuery(
					"from Order where linkType='" + linkType
							+ "' and displayed = '1'").list();
			log.info("PostsList count = " + postsList.size());
			tx.commit();
			return postsList;
		} catch (Exception e) {
			log.info("Exception error - PostDAO - getAllNotepadPosts");
			e.printStackTrace();
		}
		return postsList;
	}

	/**
	 * return the number of posts with status display = 1
	 * 
	 * @return int
	 */
	public int getOrdersNumberInDBThatAreDisplayed(int display) {
		try {
			Session session = getNewSession();

			int postCount = safeLongToInt((Long) session
					.createCriteria(Order.class)
					.add(Restrictions.eq("displayed", display))
					.setProjection(Projections.rowCount()).uniqueResult());
			return postCount;
		} catch (Exception e) {
			log.info("Exception error - PostDAO - getPostsNumberInDB");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * return the number of posts with status display = 1
	 * 
	 * @return int
	 */
	public int getOrdersNumberInDBThatAreDisplayedWithGivenLinkType(
			int display, String linkType) {
		try {
			Session session = getNewSession();

			int postCount = safeLongToInt((Long) session
					.createCriteria(Order.class)
					.add(Restrictions.eq("displayed", display))
					.add(Restrictions.eq("linkType", linkType))
					.setProjection(Projections.rowCount()).uniqueResult());
			return postCount;
		} catch (Exception e) {
			log.info("Exception error - PostDAO - getPostsNumberInDB");
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * return the number of posts with status display = 1
	 * 
	 * @return int
	 */
	public int getSpecifiedOrdersNumberInDBThatAreDisplayed(String linkType,
			int display) {
		try {
			Session session = getNewSession();

			int postCount = safeLongToInt((Long) session
					.createCriteria(Order.class)
					.add(Restrictions.eq("displayed", display))
					.add(Restrictions.eq("linkType", linkType))
					.setProjection(Projections.rowCount()).uniqueResult());
			return postCount;
		} catch (Exception e) {
			log.info("Exception error - PostDAO - getPostsNumberInDB");
			e.printStackTrace();
		}
		return 0;
	}

	// //////////////////
	// //////////////////
	// UTILS
	// //////////////////
	// //////////////////

	/**
	 * Method for casting Long to INT
	 * 
	 * @param l
	 *            long
	 * @return int l
	 */
	public int safeLongToInt(long l) {
		if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(l
					+ " cannot be cast to int without changing its value.");
		}
		return (int) l;
	}

}
