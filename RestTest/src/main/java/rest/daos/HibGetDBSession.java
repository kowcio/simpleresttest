package rest.daos;

import java.util.ArrayList;
import java.util.Set;

import javax.persistence.Entity;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.reflections.Reflections;
import org.springframework.stereotype.Service;

import rest.entities.Order;
import rest.entities.Client;



/**
 * Class retriving different sessions, sessions factories and so on.
 * @author TalentLab1
 *
 */
@Service
public class HibGetDBSession{

	private  SessionFactory sessionFactory;
	private  ServiceRegistry serviceRegistry;
	private  Session session;
	
	Logger log = Logger.getLogger("InspireLogger");
	
	
 /**
     * Get new session based on Anotations configuration
     * @return
     */
	public Session getNewSessionAnnotations(){
		
		
		Session session = null;
		try {
			Configuration config = new Configuration();

			// Sone error when mapping class, need to use hardcoded values for now	 
			
//			final Reflections reflections = new Reflections("rest.entities");
//			final Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Entity.class);
//		    for (final Class<?> clazz : classes) {
//		    	System.out.println("Dodawana klasa = "+clazz.toString());
//		        config.addAnnotatedClass(clazz);
//		    }
		  	
		    config.addAnnotatedClass(Order.class);
		    config.addAnnotatedClass(Client.class);
		    
			
			config.configure("hibernate.cfg.xml");
			SchemaExport schema = new SchemaExport(config);
      
			schema.setOutputFile("uiSchema.sql");//saved in eclipse main folder
			//schema.create(true, false);
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
		            config.getProperties()).build();
			SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
			session = sessionFactory.openSession();
	
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
		return session;
	}

	
    /**
     * Get new session based on Annotations configuration with specified hibernate.cfg.xml file <br />
     * testhibernate.cfg.xml - for testing - h2:mem:testblogdb, hbm2ddl - create
     * 
     * @return
     */
	public Session getNewSessionAnnotations(String hibfile){
		Session session = null;
		try {
			log.info("HIBGETDBSESSION ####2");
			Configuration config = new Configuration();
			config.addAnnotatedClass(Order.class);config.addAnnotatedClass(Client.class);
			config.configure(hibfile);
			SchemaExport schema = new SchemaExport(config);
			schema.setOutputFile("uiSchema.sql");//saved in eclipse main folder
			//schema.create(true, false);
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
		            config.getProperties()).build();
			SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
			session = sessionFactory.openSession();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return session;

	}
	
    /**
     * Get new session factory object based on Annotations configuration
     * @return
     */
	public SessionFactory getAnnotationsSessionFactory(){
		try {
			log.info("HIBGETDBSESSION ####3");
			Configuration config = new Configuration();
			//added annotated classes
			config.addAnnotatedClass(Order.class);config.addAnnotatedClass(Client.class);
			config.configure("hibernate.cfg.xml");
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
		            config.getProperties()).build();
			SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
			return sessionFactory;
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	/**
	 * Get a session with a specified hibernate.cfg.xml file 				<br />
	 * hib4nodb.cfg.xml - file with ifexists=false and hbm2ddl create  		<br />
	 * will create DB and table for first run if there is no DB.			<br />
	 * 
	 * @param hibcfgxmlFile
	 * @return open session
	 */
	
	public Session getNewSessionAnnotationsWithHibernateCfgXmlSession(String hibcfgxmlFile){
		Session session = null;
		try {
			log.info("HIBGETDBSESSION ####4");
			Configuration config = new Configuration();
			config.addAnnotatedClass(Order.class);config.addAnnotatedClass(Client.class);
			config.configure(hibcfgxmlFile);
			SchemaExport schema = new SchemaExport(config);
			schema.setOutputFile("uiSchema.sql");
			//schema.create(true, false);
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
		            config.getProperties()).build();
			SessionFactory sessionFactory = config.buildSessionFactory(serviceRegistry);
			session = sessionFactory.openSession();
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return session;
	}
	
	
	/**
	 * Get a session factory with a specified hibernate.cfg.xml file 				<br />
	 * hib4nodb.cfg.xml - file with ifexists=false and hbm2ddl create  		<br />
	 * will create DB and table for first run if there is no DB.			<br />
	 * 
	 * @param hibcfgxmlFile
	 * @return open session
	 */
	
	public SessionFactory getAnnotatedSessionFactorysWithSpecificHibCfgXmlFile(String hibcfgxmlFile){
		SessionFactory sessionFactory = null;
		try {
			log.info("HIBGETDBSESSION ####5");
			Configuration config = new Configuration();
			config.addAnnotatedClass(Order.class);
			config.configure(hibcfgxmlFile);
			SchemaExport schema = new SchemaExport(config);
			//schema.create(true, false);
			serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
		            config.getProperties()).build();
			sessionFactory = config.buildSessionFactory(serviceRegistry);
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sessionFactory;
	}
	
	
	
	
	

	//setters getter
	public SessionFactory getSessionFactory() {		return sessionFactory;	}
	public void setSessionFactory(SessionFactory sessionFactory) {		this.sessionFactory = sessionFactory;	}
	public ServiceRegistry getServiceRegistry() {		return serviceRegistry;	}
	public void setServiceRegistry(ServiceRegistry serviceRegistry) {		this.serviceRegistry = serviceRegistry;	}
	public Session getSession() {		return session;	}
	public void setSession(Session session) {		this.session = session;	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	

