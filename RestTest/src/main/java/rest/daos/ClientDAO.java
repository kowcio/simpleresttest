package rest.daos;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import rest.entities.Client;

/**
 * MAIN DAO class. Use contraints and etc.
 * @author Kowcio
 *
 */
public class ClientDAO {

	Logger log = Logger.getLogger("InspireLogger");
	
	HibGetDBSession fabrykaHibernejta = new HibGetDBSession();

	public Session getNewSession(){
		return fabrykaHibernejta.getNewSessionAnnotations();
	}
	/**
	 *Save a Client class  into the user table
	 *
	 * @param  client Client  
	 * @return 
	 */
	
	public Client saveClient(Client client){
	   	try {
	   		Session session = getNewSession();
			log.info("Saving Record");
			
			Transaction tx = session.beginTransaction();
			//setting the date of adding the Client
			session.saveOrUpdate(client);
			
			tx.commit();
			log.info("Client id = "+client.getId()+" added !");
			return client;
		} catch (Exception e) {
			log.info("Cos sie wysypalo - saveClient");
			 e.printStackTrace();
			 return client;
		}

	}   	
	
	
	
	/**
	 * 
	 * @return All Clients as list.
	 */
	
	
	@SuppressWarnings("unchecked")
	public List<Client> getAllClients(){
		List<Client> ClientsList = null;
	   	try {
	   		Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			ClientsList = (List<Client>)session.createQuery("from Client").list();
		   	log.info("ClientsList count = "+ClientsList.size());	
			tx.commit();
	   	} catch (Exception e) {
			log.info("Cos sie wysypalo - ClientDAO - getAllClients");
			 e.printStackTrace();
		}
	   
	   	
	   	return ClientsList;
	}

	/**
	 * Return Client by ID.
	 * 
	 * @param ClientId - Clients ID which we wish to load.
	 * @return	Specified Client
	 */
	
	
	public Client getClientByID(int ClientId){
		Client Client = new Client();
	   	try {
	   		Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			Client = (Client) session.get(Client.class, ClientId);
			tx.commit();
		   	return Client;
	   	} catch (Exception e) {
			log.info("Cos sie wysypalo - getClientByID");
			 e.printStackTrace();
			 return null;
		}
	   
		
	}

/**	Update a Client object in db.
 * 
 * @param Client Client which we wish to update. Needs to have a specified ID.
 */

	public void updateClient(Client Client){
	   	try {
	   		Session session = getNewSession();
			Transaction tx = session.beginTransaction();
			session.saveOrUpdate(Client);
			tx.commit();
	   	} catch (Exception e) {
			log.info("Cos sie wysypalo - getClientByID");
			 e.printStackTrace();
		}
	}


	/**	Delete in db a Client object by given ID.
	 * 
	 * @param Needs to have a specified ID.
	 */

		public void deleteClient(int id){
			try {
		   		Session session = getNewSession();
		   		ClientDAO Client = new ClientDAO();
				Transaction tx = session.beginTransaction();
		   		session.delete(Client.getClientByID(id));
				tx.commit();

		   	} catch (IllegalArgumentException e) {  
		   		log.info(" No Client by that id");
				 e.printStackTrace();
	        }  	catch (Exception e) {
				log.info("Cos sie wysypalo - DeleteClientByID");
			}
		}


		/**	Delete in db a Client object by given URL.
		 * @param Needs to have a specified URL.
		 */
		@Deprecated
		public void deleteClientByURLBad(String url){			
				try {
			   		Session session = getNewSession();
					Transaction tx = session.beginTransaction();
					String query = "delete from Client where url='"+url+"'";
					log.info("Delete query = "+query);
			   		session.createQuery(query);
					tx.commit();
	       }  	catch (Exception e) {
						log.info("Cos sie wysypalo - deleteClientByURL");
			    	   e.printStackTrace();
				}
				log.info("Deleted Client by URL="+url);
			}

		/**	Delete in db a Client object by given URL.
		 * @param Needs to have a specified URL.
		 * @return true if all ok 
		 */
		public boolean deleteClientByURL(String url){		
			Session session = getNewSession();
			Transaction transaction = session.beginTransaction();
			try {
			  // your code
			  String hql = "delete from Client where url= :url";
			  Query query = session.createQuery(hql);
			  query.setString("url",url);
			  log.info(query.executeUpdate());
			  // your code end

			  transaction.commit();
			  return true;
			} catch (Exception e ) {
			  transaction.rollback();
			  e.printStackTrace();
			  return false;
			}
			
			}
		
			/**	Delete in db a Client object by given URL.
			 * @param Needs to have a specified URL.
			 */
				@SuppressWarnings("unchecked")
				public Client getClientByURL(String url){
					List <Client> ml = null;
					try {
				   		Session session = getNewSession();
						Transaction tx = session.beginTransaction();
				   		log.info("Before delete");
				   		ml  = (List<Client>) session.createQuery("from Client where url='"+url+"'").list();
				   		log.info("After delete");
						tx.commit();
						return ml.get(0);
				      }  	catch (Exception e) {
						log.info("Cos sie wysypalo "+e.getCause());
						//e.printStackTrace();
						return null;
					}
					
				}
				
		/**
		 * Get number of Clients in db
		 * @param viewType
		 * @return
		 */
		public int getPostsNumberInDB( ){
			long pcl=0;
		   	try {
		   		Session session = getNewSession();
	//postCount 
				 pcl =		 (Long) session.createCriteria(Client.class)
							 .setProjection(Projections.rowCount()).uniqueResult();
			return safeLongToInt(pcl);
		   	} catch (Exception e) {
				log.info("Exception error - PostDAO - getPostsNumberInDB");
				 e.printStackTrace();
			}
		   	return 0;
		}
		
		
		
		@SuppressWarnings("unchecked")
		public List<Client> getDisplayedClients(){
			
			List<Client> motList = null;
		   	try {
		   		Session session = getNewSession();
		   		Transaction tx = session.beginTransaction();
				motList = (List<Client>)session
						.createQuery("from Client where displayed = '1'").list();
			   	log.info("motList count = "+motList.size());	
				tx.commit();
				return motList;
		   	} catch (Exception e) {
				log.info("Exception error - PostDAO - getAllNotepadPosts");
				 e.printStackTrace();
			}
		   	return null;
		}
		
		
		@SuppressWarnings("unchecked")
		public List<Client> getDisplayedSpecifiedTypeClients(String linkType){
			List<Client> postsList = null;
		   	try {
		   		Session session = getNewSession();
				Transaction tx = session.beginTransaction();
				postsList = (List<Client>)session
						.createQuery("from Client where linkType='"+linkType+"' and displayed = '1'").list();
			   	log.info("PostsList count = "+postsList.size());	
				tx.commit();
				return postsList;
		   	} catch (Exception e) {
				log.info("Exception error - PostDAO - getAllNotepadPosts");
				 e.printStackTrace();
			}
		   	return postsList;
		}
		
		
		
		/**
		 * return the number of posts with status display = 1 
		 * @return int 
		 */
		public int getClientsNumberInDBThatAreDisplayed(int display){
		   	try {
		   		Session session = getNewSession();
 
				int postCount = 
						safeLongToInt(
						 (Long) session.createCriteria(Client.class)
						.add( Restrictions.eq("displayed", display ) )
						.setProjection(Projections.rowCount()).uniqueResult()
						);
				return postCount;
		   	} catch (Exception e) {
				log.info("Exception error - PostDAO - getPostsNumberInDB");
				 e.printStackTrace();
			}
		   	return 0;
		}
		
		/**
		 * return the number of posts with status display = 1 
		 * @return int 
		 */
		public int getClientsNumberInDBThatAreDisplayedWithGivenLinkType(int display, String linkType){
		   	try {
		   		Session session = getNewSession();
 
				int postCount = 
						safeLongToInt(
						 (Long) session.createCriteria(Client.class)
						.add( Restrictions.eq("displayed", display ) )
						.add( Restrictions.eq("linkType", linkType ) )
						.setProjection(Projections.rowCount()).uniqueResult()
						);
				return postCount;
		   	} catch (Exception e) {
				log.info("Exception error - PostDAO - getPostsNumberInDB");
				 e.printStackTrace();
			}
		   	return 0;
		}
		
		/**
		 * return the number of posts with status display = 1 
		 * @return int 
		 */
		public int getSpecifiedClientsNumberInDBThatAreDisplayed(String linkType, int display){
		   	try {
		   		Session session = getNewSession();
 
				int postCount = 
						safeLongToInt(
						 (Long) session.createCriteria(Client.class)
						.add( Restrictions.eq("displayed", display ) )
						.add( Restrictions.eq("linkType", linkType ) )
						.setProjection(Projections.rowCount()).uniqueResult()
						);
				return postCount;
		   	} catch (Exception e) {
				log.info("Exception error - PostDAO - getPostsNumberInDB");
				 e.printStackTrace();
			}
		   	return 0;
		}
		////////////////////
		////////////////////
		// UTILS 
		////////////////////
		////////////////////
		
		
		/**
		 * Method for casting Long to INT
		 * @param l long
		 * @return int l
		 */
		public int safeLongToInt(long l) {
		    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
		        throw new IllegalArgumentException
		            (l + " cannot be cast to int without changing its value.");
		    }
		    return (int) l;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	

}
