package rest.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "OrderTable")
// @JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column
	private int clientID;

	@Column
	// @Length(min=3, max=40, message="Namne requires 3-40 characters")
	private Date dateOrder = new Date();

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "statusHistoryList", joinColumns = @JoinColumn(name = "shl_id"))
	@MapKeyJoinColumn(name = "id")
	private List<String> statusHistoryList = Arrays.asList("status");

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "itemsList", joinColumns = @JoinColumn(name = "il_id"))
	@MapKeyJoinColumn(name = "id")
	private List<String> itemsList = Arrays.asList("item");

	// Contructors and methods

	public Order() {
	};

	public Order(int id, List<String> status, Date dateOrder,
			List<String> itemsList, int clientID) {
		super();
		this.id = id;
		this.statusHistoryList = status;
		this.dateOrder = dateOrder;
		this.itemsList = itemsList;
		this.clientID = clientID;
	}

	public Order EmptyOrder() {
		return new Order();
	}

	// empty list stack ov
	public static Order RandomOrder() {
		Order or = new Order();
		or.setClientID(1);
		or.setDateOrder(new Date());
		or.setId(123);
		or.setItemsList(getRandomStringList("Item 1"));
		or.setStatusHistoryList(getRandomStringList("new"));
		return or;
	}

	public static List<String> getRandomStringList(String string) {
		ArrayList<String> sl = new ArrayList<String>();
		sl.add(string);
		return sl;

	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", clientID=" + clientID + ", dateOrder="
				+ dateOrder + ", statusHistoryList=" + statusHistoryList
				+ ", itemsList=" + itemsList + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<String> getStatusHistoryList() {
		return statusHistoryList;
	}

	public void setStatusHistoryList(List<String> statusHistoryList) {
		this.statusHistoryList = statusHistoryList;
	}

	public Date getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}

	public List<String> getItemsList() {
		return itemsList;
	}

	public void setItemsList(List<String> itemsList) {
		this.itemsList = itemsList;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

}
