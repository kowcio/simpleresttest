package rest.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.userdetails.UserDetails;



@Entity
@Table(name="ClientTable")
public class Client {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(unique=true, nullable = false)
    private int id;
	
	@Length(min=3, max=100, message="Adress requires 3-100 characters - in database constraints")
	private String address="Default address";
	
	@Pattern(regexp="^[A-Za-z -]{3,30}$",message="Username must match : A-Za-z, 0-9,' ', -, 3-30 chars")
	@Length(min=3, max=40, message="Namne requires 3-40 characters")
    private String name="Defaul name";
	
	@Pattern(regexp="^[A-Za-z -]{3,30}$",message="Username must match : A-Za-z, 0-9,' ', -, 3-30 chars")
	@Length(min=3, max=40, message="Surname requires 3-40 characters")
	private String surname ="Default surname";
	
		
	public Client(){};
	
	
	public Client(int id, String address, String name, String surname) {
		super();
		this.id = id;
		this.address = address;
		this.name = name;
		this.surname = surname;
	}

	


	public Client NewBlankUser(){
		return new Client();
	}



	@Override
	public String toString() {
		return "Client [id=" + id + ", address=" + address + ", name=" + name
				+ ", surname=" + surname + "]";
	}
	

	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getAddress() {
		return address;
	}




	public void setAddress(String address) {
		this.address = address;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getSurname() {
		return surname;
	}




	public void setSurname(String surname) {
		this.surname = surname;
	}
	
}
	
	
	