package rest.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.hibernate.SessionFactory;

import rest.daos.HibGetDBSession;

/**
 * Class to configure database with basic new data
 * 
 */

public class DBConfig implements ServletContextListener {

	String hibfile = "hibernate.cfg.xml";

	@Override
	public void contextInitialized(ServletContextEvent arg0) {

		System.out
				.println(""
						+ "############   CREATING DATABASE FOR THE INITIAL RUN   ############ \n"
						+ "############   CREATING DATABASE FOR THE INITIAL RUN   ############ \n"
						+ "############   CREATING DATABASE FOR THE INITIAL RUN   ############ "
						+ "\n http://forum.springsource.org/archive/index.php/t-29874.html");

		// First session will create DB
		// get basic sessionFact only if db is created
		HibGetDBSession hibs = new HibGetDBSession();
		SessionFactory sf1 = hibs
				.getAnnotatedSessionFactorysWithSpecificHibCfgXmlFile("hibernate.cfg.xml");

		// Manage if session can actualy get something.

	}

	// METHODS

	/**
	 * Save initials posts , Motivators to the database while creating it or
	 * inspecting if there is not enough information inside it <br />
	 * 
	 * @param sf
	 *            - Session Factory for creating sessions inside methods and
	 *            DAOs.
	 */

	public String saveInitialDBData(SessionFactory sf) {
		saveSingleInitialMotivators(sf);
		return "OK";
	}

	public String saveSingleInitialMotivators(SessionFactory sf) {
		return hibfile;

		// TODO;
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	public String getHibfile() {
		return hibfile;
	}

	/**
	 * set the hibernate file for DB <br />
	 * hibernate.cfg.xml <br />
	 * testhibernate.cfg.xml <br />
	 * 
	 * @param hibfile
	 */
	public void setHibfile(String hibfile) {
		this.hibfile = hibfile;
	}

}
