package rest.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import rest.daos.OrderDAO;
import rest.entities.Order;

@Service
public class OrderServiceImpl implements OrderService {

	Logger log = Logger.getLogger("InspireLogger");

	@Override
	public Order getRandomOrder() {

		Order or = Order.RandomOrder();
		return or;
	}

	@Override
	public Order saveOrder(Order order) {
		OrderDAO odao = new OrderDAO();
		String statusString = order.getStatusHistoryList().toString();
		List<String> shl = new ArrayList<String>();
		shl.add(addDateToStatus(statusString));
		order.setStatusHistoryList(shl);
		odao.saveOrder(order);
		return order;

	}

	@Override
	public Order getStatus(int id) {
		OrderDAO odao = new OrderDAO();
		Order order = odao.getOrderByID(id);

		return order;
	}

	@Override
	public Order cancelOrder(Order order) {
		// TODO Auto-generated method stub
		return Order.RandomOrder();
	}

	@Override
	public Order modifyOrder(int id, String status) {

		OrderDAO odao = new OrderDAO();
		Order order = odao.getOrderByID(id);
		String strDate = addDateToStatus(status);
		List<String> shl = order.getStatusHistoryList();

		String check = shl.get(shl.size() - 1).split("Status:")[1].trim();

		log.info("checked = " + status);
		if (check.equals("canceled")) {
			log.info("cancel modyfing order");

			return order;

		} else {
			// shl.add(strDate);
			// order.setStatusHistoryList(shl);
			order.getStatusHistoryList().add(strDate);
			odao.updateOrder(order);
			log.info("modyfing order");

			return order;
		}
	}

	@Override
	public Order getOrderByID(int id) {
		OrderDAO odao = new OrderDAO();
		Order order = odao.getOrderByID(id);
		return order;
	}

	// TODO
	@Override
	public Order modifyList(int id, String list) {
		OrderDAO odao = new OrderDAO();
		Order order = getOrderByID(id);
		// List<String> il = order.getItemsList();
		order.getItemsList().add(list);
		odao.updateOrder(order);

		return order;
	}

	/**
	 * Add date information to status and return as string
	 * 
	 * @param status
	 * @return
	 */
	private String addDateToStatus(String status) {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// dd/MM/yyyy
		String strDate = sdfDate.format(new Date());
		strDate = "#Date:" + strDate + "-Status:" + status;
		return strDate;
	}

}