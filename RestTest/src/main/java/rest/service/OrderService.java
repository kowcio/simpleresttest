package rest.service;

import rest.entities.Order;

public interface OrderService {
	
	public Order getRandomOrder();
	
	public Order saveOrder(Order order);
	
	public Order  getStatus(int id);

	public Order cancelOrder(Order order);
	
	public Order modifyOrder(int id, String status);
	
	public Order modifyList(int id, String list);

	public Order getOrderByID(int id);
	
	
}