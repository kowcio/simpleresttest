package rest.Tests;

import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import rest.daos.OrderDAO;
import rest.entities.Order;
import rest.service.OrderServiceImpl;

@Test
public class RestTests {

	Logger log = Logger.getLogger("InspireLogger");
	
	@Test
	public void saveOrderByServiceAndDeleteByDAOTest(){
		
		OrderServiceImpl osl = new OrderServiceImpl();
		
		List<String> sl = Arrays.asList("ItemsTestList");
		List<String> shl = Arrays.asList("Status History List");
		
		Order testOrder = new Order( 0, sl, new Date(),  shl , 1234);
		
		OrderDAO odao = new OrderDAO();
		int savedOrderID = osl.saveOrder(testOrder).getId();
		log.info("Saved order has ID = " + savedOrderID);
		
		Order checkOrder = odao.getOrderByID(savedOrderID);
		
		Assert.assertEquals(testOrder.getClientID() , checkOrder.getClientID() );
		
		//cleaning DB 
		odao.deleteOrder(savedOrderID);
		
		
		Order checkOrder2 = odao.getOrderByID(savedOrderID);
		
		Assert.assertEquals(null, checkOrder2);
		
		
	}
	
	@Test
	public void ModifyOrderByServiceTest(){
		
		OrderServiceImpl osl = new OrderServiceImpl();
		OrderDAO odao = new OrderDAO();
		List<String> il = Arrays.asList("ItemsTestList");
		List<String> shl = Arrays.asList("Status History List");
		Order testOrder = new Order( 0, il, new Date(),  shl , 12345);
		
		int savedOrderID = osl.saveOrder(testOrder).getId();
		log.info("Saved order has ID = " + savedOrderID);
		Order checkOrder = odao.getOrderByID(savedOrderID);
		
		for(int i = 0 ; i<=0 ; i++){
			log.info( i + " - " + il.get(i) ) ;
		}
		
		//modify order
		checkOrder.getItemsList().add("Added Item");
		odao.updateOrder(checkOrder);
		List<String> il2 = osl.getOrderByID(savedOrderID).getItemsList();
		
		for(int i = 0 ; i< 2 ; i++){
			log.info( i + " - " + il2.get(i) ) ;
		}
		
		Assert.assertEquals(il2.size() , 2 );
		//cleaning DB 
		odao.deleteOrder(savedOrderID);
		Order checkOrder2 = odao.getOrderByID(savedOrderID);
		Assert.assertEquals(null, checkOrder2);
	
	}
	
	
	
	@Test
	public void SeleniumWebPageTest(){
		String context = "/RestTest/";
		String url = "127.0.0.1:8080"+context;//root with form to login or main index page
		int timeout 		=  5000;					//ms for input fields 
		String timePageLoad = "2500";				//ms
		String ulog = "admin";						//user for tests login
		String upass = "admin";						//user for tests password
		DesiredCapabilities caps = DesiredCapabilities.chrome()	;
		
		//Mogą występować problemy z zajęciem portu przez FF.
		WebDriver driver =  new FirefoxDriver();
		
        driver.navigate().to(url);;	

        WebElement ad = driver.findElement(By.id("address"));
        ad.sendKeys("Test user");
        
        WebElement name = driver.findElement(By.id("name"));
        ad.sendKeys("Test user");
        
        WebElement surname = driver.findElement(By.id("surname"));
        ad.sendKeys("Test user");
        		
        driver.findElement(By.id("submitClient")).click();
		
        WebElement response = driver.findElement(By.id("response"));
		//check if we got any reponsde in the #response div.
		Assert.assertNotNull(response);
		
		
	
	
	}
	
	
	
	
	
}
